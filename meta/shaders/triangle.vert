#version 330 core

layout(location = 0) in vec3 vert_position;

out vec2 pos;

void main()
{
    gl_Position.xyz = vert_position;
    gl_Position.w = 1.0;
    pos = vec2(vert_position.x, vert_position.y);
}
