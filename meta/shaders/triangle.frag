#version 330 core
precision mediump float;

uniform float time;
out vec4 out_color;

in vec2 pos;

#define SPEED(T) (T * 2.0)

void main() {
   float r = (cos((SPEED(time)) + pos.x) + 1.0) / 2.0;
   float g = (sin((SPEED(time)) + pos.x) + 1.0) / 2.0;
    out_color = vec4(
        r,
        g,
        (cos((r + g)) + 1.0) / 2.0,
        1.0
    );
}
