#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/signal.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "logging.h"
#include "shaders.h"

struct opengl_setup_config {
    uint8_t version_major, version_minor;
    // Anti-aliasing Samples (x2, x4)
    uint8_t aa_samples;
    bool vsync_enabled;
    // Default window name
    const char *window_name;
} __attribute__((__packed__));

GLFWwindow *opengl_init(struct opengl_setup_config config) {
    if (!glfwInit())
        die("could not initialize GLFW");

    glfwWindowHint(GLFW_SAMPLES, config.aa_samples);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, config.version_major);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, config.version_minor);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow *window;
    if (!(window = glfwCreateWindow(800, 600, config.window_name, NULL, NULL)))
        die("could not create GLFW window");

    glfwMakeContextCurrent(window);
    if (glewInit() != GLEW_OK)
        die("could not initialize GLEW");

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glfwSwapInterval(config.vsync_enabled ? 1 : 0);
    glClearColor(0.10f, 0.10f, 0.10f, 1.0f);

    const char *version = (const char *)glGetString(GL_VERSION);
    info("OpenGL version: %s", version);
    return window;
}

void opengl_free(GLFWwindow *window) {
    info("cleaning up OpenGL");
    glfwDestroyWindow(window);
    glfwTerminate();
}

static GLuint prog;
static void compile_shaders(void) {

    shaders_compile(&prog, "meta/shaders/triangle.vert", "meta/shaders/triangle.frag");
    glUseProgram(prog);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_F5 && action == GLFW_PRESS) {
        compile_shaders();
    }

    (void)window;
    (void)scancode;
    (void)mods;
}

/* clang-format off */
static const GLfloat vert_buf_data[] = {
   -0.5f, -0.5f,  0.0f,
    0.5f, -0.5f,  0.0f,
    0.0f,  0.5f,  0.0f,
};
/* clang-format on */

static bool keep_running = true;
void exit_handler(int dummy) {
    keep_running = false;
    (void)dummy;
}

int main(void) {
    signal(SIGINT, exit_handler);
    signal(SIGTERM, exit_handler);

    GLFWwindow *window = opengl_init((struct opengl_setup_config){
        .window_name = "Zion",
        .version_major = 3,
        .version_minor = 3,
        .vsync_enabled = true,
        .aa_samples = 4,
    });

    compile_shaders();

    GLuint time_uniform = glGetUniformLocation(prog, "time");

    int width, height;
    GLuint vao_id;
    glGenVertexArrays(1, &vao_id);
    glBindVertexArray(vao_id);

    GLuint vert_buf;
    glGenBuffers(1, &vert_buf);
    glBindBuffer(GL_ARRAY_BUFFER, vert_buf);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vert_buf_data), vert_buf_data, GL_STATIC_DRAW);

    glfwSetKeyCallback(window, key_callback);

    while (keep_running && !glfwWindowShouldClose(window)) {
        glfwGetFramebufferSize(window, &width, &height);
        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glUniform1f(time_uniform, glfwGetTime());

        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vert_buf);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

        glDrawArrays(GL_TRIANGLES, 0, 3);
        glDisableVertexAttribArray(0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    opengl_free(window);
    return 0;
}
