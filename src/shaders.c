#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logging.h"
#include "shaders.h"

const char *read_file(const char *fp) {
    FILE *f = fopen(fp, "rb");
    if (f == NULL)
        return NULL;

    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);

    char *string = malloc(fsize + 1);
    fread(string, fsize, 1, f);
    fclose(f);

    string[fsize] = 0;
    return string;
}

typedef enum {
    COMPILATION,
    LINKING,
} assert_shader_type;
void assert_shader(assert_shader_type type, GLuint id, const char *err) {
    int len = 0;
    GLint result = GL_FALSE;
    char gl_err_buf[1024] = {0};

    switch (type) {
    case COMPILATION: {
        glGetShaderiv(id, GL_COMPILE_STATUS, &result);
        glGetShaderiv(id, GL_INFO_LOG_LENGTH, &len);
        if (result == GL_FALSE)
            glGetShaderInfoLog(id, len, NULL, gl_err_buf);
    } break;
    case LINKING: {
        glGetProgramiv(id, GL_LINK_STATUS, &result);
        glGetProgramiv(id, GL_INFO_LOG_LENGTH, &len);
        if (result == GL_FALSE)
            glGetProgramInfoLog(id, len, NULL, gl_err_buf);
    } break;
    }

    if (result == GL_FALSE)
        die("%s: %s", err, gl_err_buf);
}

void shaders_compile(GLuint *program, const char *vert_shader_path, const char *frag_shader_path) {
    const char *vert_shader_code = read_file(vert_shader_path);
    if (!vert_shader_code)
        die("vertex shader path error:");

    const char *frag_shader_code = read_file(frag_shader_path);
    if (!frag_shader_code)
        die("fragmen shader path error:");

    GLuint vert_shader_id = glCreateShader(GL_VERTEX_SHADER);
    GLuint frag_shader_id = glCreateShader(GL_FRAGMENT_SHADER);

    info("compiling vertex shader `%s`", EXTRACT_FILE_NAME(vert_shader_path));
    glShaderSource(vert_shader_id, 1, &vert_shader_code, NULL);
    glCompileShader(vert_shader_id);
    assert_shader(COMPILATION, vert_shader_id, "vertex shader error");

    info("compiling fragment shader `%s`", EXTRACT_FILE_NAME(frag_shader_path));
    glShaderSource(frag_shader_id, 1, &frag_shader_code, NULL);
    glCompileShader(frag_shader_id);
    assert_shader(COMPILATION, frag_shader_id, "fragment shader error");

    info("linking vertex & fragment shaders");
    *program = glCreateProgram();
    glAttachShader(*program, vert_shader_id);
    glAttachShader(*program, frag_shader_id);
    glLinkProgram(*program);
    assert_shader(LINKING, *program, "program linking error");

    glDeleteShader(frag_shader_id);
    glDeleteShader(vert_shader_id);

    free((void *)frag_shader_code);
    free((void *)vert_shader_code);
}
