#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logging.h"

const char *sev_to_cstr(uint8_t sev) {
    switch (sev) {
    case 1:
        return "\x1b[33;1mWarn\x1b[0m";
    case 2:
        return "\x1b[31;1mError\x1b[0m";
    case 0:
    default:
        return "\x1b[32;1mInfo\x1b[0m";
    }
}

void log_impl(const char *file, uint32_t line, uint8_t sev, const char *fmt, ...) {
    char buf[1024] = {0};

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, sizeof(buf), fmt, ap);
    va_end(ap);

    fprintf(stderr, "\x1b[2m%s:%d\x1b[0m ", EXTRACT_FILE_NAME(file), line);
    fprintf(stderr, "%s %s\n", sev_to_cstr(sev), buf);
}

void die(const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    char buf[1024];
    int buf_len = vsnprintf(buf, sizeof(buf), fmt, ap);
    va_end(ap);

    fprintf(stderr, "\x1b[31;1mERROR:\x1b[0m %s", buf);
    if (buf_len > 0 && buf[buf_len - 1] == ':') {
        fputc(' ', stderr);
        perror(NULL);
    } else {
        fputc('\n', stderr);
    }
    exit(1);
}
