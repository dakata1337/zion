#ifndef _ZION_LOGGING_H_
#define _ZION_LOGGING_H_

#include <stdint.h>

#define EXTRACT_FILE_NAME(FILE) (strrchr(FILE, '/') ? strrchr(FILE, '/') + 1 : FILE)

#define info(...)               log_impl(__FILE__, __LINE__, 0, __VA_ARGS__)
#define warn(...)               log_impl(__FILE__, __LINE__, 1, __VA_ARGS__)
#define error(...)              log_impl(__FILE__, __LINE__, 2, __VA_ARGS__)

void log_impl(const char *file, uint32_t line, uint8_t sev, const char *fmt, ...);
void die(const char *fmt, ...);

#endif // _ZION_LOGGING_H_
