#ifndef _ZION_SHADERS_H_
#define _ZION_SHADERS_H_

#include <GL/glew.h>

void shaders_compile(GLuint *program, const char *vert_shader_path, const char *frag_shader_path);

#endif // _ZION_SHADERS_H_
